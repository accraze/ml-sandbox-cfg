#!/bin/bash
#######################################
# Create dev charts via helm template #
#######################################
git clone "https://gerrit.wikimedia.org/r/operations/deployment-charts"
cd deployment-charts
helm template "charts/knative-serving" > dev-knative-serving.yaml
helm template "charts/kserve" > dev-kserve.yaml

# replace all references to "RELEASE_NAME" to "dev"
sed -i 's/RELEASE-NAME/dev/g' dev-knative-serving.yaml
sed -i 's/RELEASE-NAME/dev/g' dev-kserve.yaml

######################
# Istio Installation #
######################
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
  name: istio-system
  labels:
    istio-injection: disabled
EOF

/usr/bin/istioctl-1.9.5 manifest apply -f ../istio-minimal-operator.yaml -y

########################
# Knative Installation #
########################
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
  name: knative-serving
  labels:
    serving.knative.dev/release: "v0.18.1"
EOF

kubectl apply -f charts/knative-serving-crds/templates/crds.yaml

kubectl apply -f dev-knative-serving.yaml

# update config-deployment to skip tag resolving
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: config-deployment
  namespace: knative-serving
data:
  queueSidecarImage: docker-registry.wikimedia.org/knative-serving-queue:0.18.1-4
  registriesSkippingTagResolving: "kind.local,ko.local,dev.local,docker-registry.wikimedia.org"
EOF

#######################
# KServe Installation #
#######################
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Namespace
metadata:
  labels:
    control-plane: kserve-controller-manager
    controller-tools.k8s.io: "1.0"
    istio-injection: disabled
  name: kserve
EOF

kubectl apply -f dev-kserve.yaml

# delete  existing certs
kubectl delete secret kserve-webhook-server-cert -n kserve
kubectl delete secret kserve-webhook-server-secret -n kserve

curl -LJ0 https://raw.githubusercontent.com/kserve/kserve/master/hack/self-signed-ca.sh > self-signed-ca.sh
chmod +x self-signed-ca.sh
./self-signed-ca.sh

kubectl create namespace kserve-test
